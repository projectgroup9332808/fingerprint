#include "CImg.h"

using namespace cimg_library;

//a function that applies a mask on a rectangular region
void apply_mask(CImg<unsigned char>& img, const CImg<unsigned char>& mask, const CImg<unsigned char>& struct_elem) {
    CImg<unsigned char> copy_img(img);
    copy_img.erode(struct_elem);
    cimg_forXY(mask, x, y) {
        if (mask(x, y) == 1) {
            img(x, y) = copy_img(x, y);
        }
    }
}

int main() {

    // loading the images

    CImg<unsigned char> cleanf("clean_finger.png");
    CImg<unsigned char> moistf("moist_finger.png");
    CImg<unsigned char> dryf("dry_finger.png");

    CImg<unsigned char> cleanf_copy(cleanf);
    CImg<unsigned char> moistf_copy(moistf);
    CImg<unsigned char> dryf_copy(moistf);

    //finding the threshold value using the otsu method

    int clean_thresh = cleanf_copy.otsu();
    int moist_thresh = moistf_copy.otsu();
    int dry_thresh = dryf_copy.otsu();

    // applying the binarization

    CImg<unsigned char> bcleanf = cleanf.threshold(clean_thresh);
    CImg<unsigned char> bmoistf = moistf.threshold(moist_thresh);
    CImg<unsigned char> bdryf = dryf.threshold(moist_thresh);

    //display the binary images
    
    (bcleanf, bmoistf, bdryf).display("Binarization of clean, moist and dry finger");

    // Perform dilation

    // Define a structuring element 
    CImg<unsigned char> struct_elem(3,3,1,1,0);

    struct_elem(1,0) = 1;
    struct_elem(1,1) = 1;
    struct_elem(0,1) = 1;
    struct_elem(2,1) = 1;
    struct_elem(1,2) = 1;

    CImg<unsigned char> copibcleanf(bcleanf);
    CImg<unsigned char> copi2bcleanf(bcleanf);
    copi2bcleanf = bcleanf.get_dilate(struct_elem);

    CImg<unsigned char> se(3,3,1,1,0);
    se.fill(1);
    copibcleanf.dilate(se);

    CImg<unsigned char> se2(2,2,1,1,0);
    se2.fill(1);

    CImg<unsigned char> se3(1,1,1,1,0);
    se3.fill(1);
    copibcleanf.dilate(struct_elem);
    copibcleanf.dilate(se2);
    copibcleanf.dilate(se3);

    // Create a binary mask to exclude certain parts of the image
    CImg<unsigned char> maskd(bcleanf.width(), bcleanf.height(), 1,1,0);
    cimg_forY(maskd, y) {
        cimg_forX(maskd, x) {
            if (x > 5 && x < 250 && y > 23 && y < 173) {
                maskd(x,y) = 1;
            }
        }
    }

    // Copy pixels from the dilated image to the original image using the binary mask
    cimg_forY(maskd, y) {
        cimg_forX(maskd, x) {
            if (x > 5 && x < 250 && y > 23 && y < 173) {
                if (maskd(x, y) == 1) {
                    copi2bcleanf(x, y) = copibcleanf(x, y);
                }
            }
        }
    }

    // Display the result
    (bcleanf,copi2bcleanf,bdryf).display("Dilation of clean finger to have dry finger");


     // create masks where we want to apply erosion
    CImg<unsigned char> mask2(bcleanf.width(), bcleanf.height(), 1, 1, 0), mask3(bcleanf.width(), bcleanf.height(), 1, 1, 0),
        mask4(bcleanf.width(), bcleanf.height(), 1, 1, 0), mask5(bcleanf.width(), bcleanf.height(), 1, 1, 0);
    cimg_forXY(mask2, x, y) {
        if (x > 65 && x < 184 && y > 146 && y < 287) {
            mask2(x, y) = 1;
        }
    }
    cimg_forXY(mask3, x, y) {
        if (x > 55 && x < 150 && y > 83 && y < 144) {
            mask3(x, y) = 1;
        }
    }
    cimg_forXY(mask4, x, y) {
        if (x > 157 && x < 216 && y > 124 && y < 204) {
            mask4(x, y) = 1;
        }
    }
    cimg_forXY(mask5, x, y) {
        if (x > 104 && x < 161 && y > 250 && y < 286) {
            mask5(x, y) = 1;
        }
    }

    // apply masks
    apply_mask(bcleanf, mask2, struct_elem);
    apply_mask(bcleanf, mask3, struct_elem);
    apply_mask(bcleanf, mask4, struct_elem);
    apply_mask(bcleanf, mask5, struct_elem);

    // display results
  
    (cleanf,bcleanf, bmoistf).display("Erosion of clean finger to have moist finger");

   //performing closing and opening

   //struct elem for opening and closing
    CImg<unsigned char> se_oc(3,3,1,1,0);
    se_oc.fill(1);

    CImg<unsigned char> closed_bdryf = (bdryf).get_closing(se_oc);
    CImg<unsigned char> opened_bdryf = (bdryf).get_opening(se_oc);

    (bdryf, closed_bdryf, opened_bdryf).display("Closing and opening on dry finger"); 


    
    return 0;
}
