

#include "Patch.h"




/*
    Implementation of Patch
*/



/*
The Constructor
*/
Patch::Patch(int PatchSize, CImg<unsigned int> img, unsigned int xAxis, unsigned int yAxis) : size(PatchSize)
{
    x = xAxis;
    y = yAxis;
    
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            patch.push_back(img(-(size-1)/2 + i + x, -(size-1)/2 + j + y));
        }
    }
}




/*
Compute the distance between two patches using only available pixels.
We use the Euclidian distance, and as a commentary we define the manhattan distance.
*/
int Patch::distance(Patch P, CImg<unsigned int> &mask)
{
    int dist = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size; j++)
        {
            if (mask(-(size-1)/2 + i + x, -(size-1)/2 + j + y) == 0)
            {
                /*
                We return the minimum distance from dist1 and dist2, the idea here is to also compute the distance with the symmetrical patch, it is equivalent to having a double sized dictionary. 
                We use this to fix the lack of stripes in the dictionary
                */
                dist += (P.patch[size*i + j] - patch[size*i + j])*(P.patch[size*i + j] - patch[size*i + j]);
                /*
                dist2 += (P.patch[size*(size-i-1) + j] - patch[size*i + j])*(P.patch[size*(size-i-1) + j] - patch[size*i + j]);
                */
            }
        }
    }
    return dist;
}



/*
Finds the closest Patch within the dictionary
*/
Patch Patch::closestPatch(map<int, Patch> dict, CImg<unsigned int> &mask)
{
    unsigned int size = dict.size();
    Patch P = dict[0];
    unsigned int distMin = distance(dict[0], mask);
    for (int i = 1; i < (int)size; i++)
    {
        int dist = distance(dict[i], mask);
        if (dist < (int)distMin)
        {
            distMin = dist;
            P = dict[i];
        }
    }
    P.x = x;
    P.y = y;
    return P;
}


/*
Constructor with no entries to avoid errors
*/
Patch::Patch()
{
}


/*
Last step, inpaint the pixel with the middle pixel in the closest patch
*/
void Patch::fillWith(CImg<unsigned int> &img, CImg<unsigned int> &mask)
{
    
    img(x, y) = patch[size*((size-1)/2) + (size-1)/2];
    
    mask(x, y) = 0;
    
    
}

