#include "CImg.h"
#ifdef Success
  #undef Success
#endif
#include "./eigen/Eigen/Dense"
#include "Image.h"

#ifndef __UTILS_H_
#define __UTILS_H_

// Computes the radius from the center of pressure for isotropic functions
float dist(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2);

// Computes the distance from the center of pressure for anisotropic functions
float aniso_dist(unsigned int x1, unsigned int y1, unsigned int x2, unsigned int y2, float a, float b);

#endif

