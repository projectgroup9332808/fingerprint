



#include "Masque.h"




/*
    Implementation of Masque
*/




/*
Class Masque contains : - mask : the binary image with white pixels (=255) for the pixels to inpaint .. 
                        - xOrigin, yOrigin : The center coordinates of the ellipse.
                        - xRadius, yRadius : The parameters of the ellipse (shape of the mask)
                        - PatchSize : the size of the patch
                        
*/



/*
Constructor
*/
Masque::Masque(int patchSize, int width, int height, int x, int y, int xRad, int yRad): xOrigin(x), yOrigin(y), xRadius(xRad), yRadius(yRad), PatchSize(patchSize)
{
    mask = CImg<unsigned int>(width, height, 1, 1);
    for (int i =  0; i < width; i++)
    {
        for (int j = 0; j < height; j++)
        {
            mask(i, j) = 0;
        }
    }
    for (float i =  0; i < width; i++)
    {
        for (float j = 0; j < height; j++)
        {
            if (pow((i-x)/xRad, 2) + pow((j-y)/yRad, 2) >= 1 and pow((i-x)*2/(width), 2) + pow((j-y)*2/(height), 2) < 1)
            {
                mask(i, j) = 255;
            }
        }
    }
}

