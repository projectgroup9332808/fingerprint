#include <iostream>
#include <cmath>
#include "Coord.h"
//Overloadinf assign operator 
Coord& Coord::operator=(const Coord& c){
	x = c.x;
	y = c.y;
	return *this;
}
//Overloading ostream operator
std::ostream& operator<<(std::ostream& os, const Coord& c){
	os <<"x = "<< c.x << " y = "<<c.y<<"\n"; 
	return os;
}
Coord::Coord() : x(0), y(0) {}

Coord::Coord(float x, float y) : x(x), y(y) {}

Coord::Coord(unsigned int x, unsigned int y) : x((float)x), y((float)y){}

void Coord::rotation(float theta, unsigned int x0, unsigned int y0)
{
	x = x0 + cos(theta) * (x - x0) + (y - y0) * sin(theta);
	y = y0 - sin(theta) * (x - x0) + (y - y0) * cos(theta);
}
//Setters
void Coord::Set_x(float t){
	x = t;
}

void Coord::Set_y(float t){
	y = t;
}

//Getters
float Coord::Get_x()
{
	return x;
}
float Coord::Get_y()
{
	return y;
}

void Coord::translation(float scalar, unsigned int x0, unsigned int y0){
	x += scalar * x0;
	y += scalar * y0;
}

void Coord::translation(float scalar){
	x += scalar;
	y += scalar;
}

void Coord::warp(float theta, float scalar, unsigned int x0, unsigned int y0){
	rotation(theta, x0, y0);
	translation(scalar, x0, y0);
}
