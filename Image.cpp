#include <iostream>
#include "Image.h"
#include <string>
#include <cassert>
#include <cstdlib>
#include <random>
#ifdef Success
  #undef Success
#endif
#include "./eigen/Eigen/Dense"
#include "Utils.h"
#include "Coord.h"

using namespace std;
using namespace cimg_library;




/*
 *constructor
 *@param filename the name of the Image to initialize the attribute with
 */
Image::Image(const char *const filename)
{    //Image.spectrum(1);
    CImg<unsigned int> image(filename);
    Im = image;
    width = image.width();
    height = image.height();
}

/*
 * Returns the maximum intensity value of the Image
 */
unsigned int Image::maxIntensity(){
    return (Im.max());
}

/*
 * Returns the minimal intensity value of the Image
 */
unsigned int Image::minIntensity()
{
    return (Im.min());
}

/*
 * Getter that returns the width of the Image
 */
unsigned int Image::get_width(){
	return width;
}

/*
 * Getter that returns the height of the Image
 */
unsigned int Image::get_height(){
	return height;
}

//overloading operator()
CImg<unsigned int> Image::operator()(){
	return Im;
}

//Casting

CImg<float> Image::casting_int_float()
{
    CImg<float> converted(width, height, 1, 1);
    cimg_forXYC(Im,x,y,c){
   	converted(x,y,c) =(Im(x,y,c) / (255.0));
   }
   return converted;
}

void Image::casting_float_int(const CImg<float>& image)
{
    cimg_forXYC(image,x,y,c){
    	Im(x,y,c) = image(x,y,c)*(255.0);
    }

}


/*
 * Draws a rectangle on the Image
 *@param x: x_coord of the starting point of the rectangle
 *@param y: y_coord of the starting point of the rectangle
 *@param h: the height of the rectangle
 *@param w: the width of the rectangle to draw
 *@param color: the color of the rectangle to draw
 */
void Image::Rectangle(unsigned int x, unsigned int y, unsigned int h, unsigned int w, unsigned int color){
  for(unsigned int i = x; i < x+w; i++){
      for(unsigned int j = y; j < y+h; j++){
        Im(i,j,0,0) = color;
      }
    }
    Im.save("rectangle.png"); //saves the new image
}

/*  Visualize(float theta, float scalar)
*inputs: the angle of rotation of the image,the scalar of translation

role: it performs the rotaton of angle theta and also the translation with respect to the diagonale.
      hence the need of use of the class Coord

output:
*/
Coord** Image::Visualize(float theta, float scalar) {
        CImg<unsigned int> newIm(width,height,1,1,"255",true);
        Coord **Store_matrix = new Coord*[height];
        for(unsigned int i = 0; i < height; i++)
        {
                Store_matrix[i] = new Coord[width];
        }

        for(unsigned int y = 0; y < height; y++){
                for (unsigned int x = 0; x < width; x++){
                        Coord c = Coord(x,y);
                        c.rotation(theta, width/2, height/2);//uses the rotation function to rotate the coordinates of pixels
                        c.translation(scalar, width, height);//uses the translation function to translate the coordinates of pixels
                        if(c.Get_x()<width && c.Get_y()<height){
                                newIm(c.Get_x(), c.Get_y()) = Im(x,y,0,0);//fills the matrix wih the intensity values of the image
                        	Store_matrix[y][x] = c;
			}
                }
        }
        //part of getting the lost pixels in rotation and translation
        for(unsigned int y = 1; y < height-1; y++)
        {
                for (unsigned int x = 1; x < width-1; x++)
                {
                    if(newIm(x-1,y,0,0)!=255 && newIm(x+1,y,0,0)!=255 && newIm(x,y-1,0,0)!=255 && newIm(x,y+1,0,0)!=255)
                        newIm(x,y,0,0)=(newIm(x-1,y,0,0)+newIm(x+1,y,0,0)+newIm(x,y-1,0,0)+newIm(x,y+1,0,0))/4;
                }
        }

        newIm.save("after_rotation1.png");
        //newIm.display();
	return Store_matrix;
}

/*
 * Rotates and translates the inout image and returns the transformed image
 *@param old_coord: the coordinates of the input image
 *@param old_values: the pixel values of the intial image
 *@param new_coord: the new transformed image
 */

CImg<unsigned int> Image_Warp(Coord **old_coord, CImg<unsigned int> old_values, Coord **new_coord){
	unsigned int width = old_values.width();
	unsigned int height = old_values.height();
	//test
	CImg<unsigned int> newIm(width,height,1,1,"255",true);
	for(unsigned int y = 0; y < height; y++)
	{
		for(unsigned int x = 0; x < width; x++)
		{
			if(old_coord[y][x].Get_x()<width && old_coord[y][x].Get_y()<height){
                                newIm(new_coord[y][x].Get_x(), new_coord[y][x].Get_y()) = old_values(x, y, 0, 0);//fills the matrix wih the intensity values of the image
        		}
		}

	}

	newIm.save("image_warp.png");
	return newIm;
}


/*
 * Operate the inverse rotation on the rotated Image
 *@param * rotated_Im: the initial image
 *@param theta: the angle of rotation
 *@param scalar: the coefficient of translation
 */

void Inverse_warp(CImg<unsigned int> rotated_Im,float theta, float scalar)
{

  CImg<unsigned int> newIm(rotated_Im.width(),rotated_Im.height(),1,1,"255",true);
  for(unsigned int x=0 ; x < (unsigned int)newIm.width() ; x++)
  {
    for(unsigned int y=0 ; y<(unsigned int)newIm.height(); y++)
    { 
      Coord c = Coord(x,y);
      c.translation(-scalar, newIm.width(), newIm.height());
      c.rotation(-theta, newIm.width()/2, newIm.height()/2);
      if(0<c.Get_x() && c.Get_x()<newIm.width() && 0<c.Get_y() &&  c.Get_y()< newIm.height())
      newIm(c.Get_x(),c.Get_y(),0,0) = rotated_Im(x,y,0,0);//fills the matrix
    }
  }
  for(int y = 1; y < rotated_Im.height()-1; y++)
  {
          for (int x = 1; x < rotated_Im.width()-1; x++)
          {
              if(newIm(x-1,y,0,0)!=255 && newIm(x+1,y,0,0)!=255 && newIm(x,y-1,0,0)!=255 && newIm(x,y+1,0,0)!=255)
                  newIm(x,y,0,0)=(newIm(x-1,y,0,0)+newIm(x+1,y,0,0)+newIm(x,y-1,0,0)+newIm(x,y+1,0,0))/4;
          }
  }
  newIm.save("saved_inverse.png");
}

/**
 * Displays and saves the Image to which the isotropic function 2/(1+exp(alph*r)) is  applied.
 * @param x the x coordinate of the center of pressure.
 * @param y the y coordinate of the center of pressure.
 * @param alpha the coefficient to tune the function
*/

void Image::Iso_func1(unsigned int x, unsigned int y, unsigned int alpha)
{
	Eigen::MatrixXf values(width, height);
	// draw the center of pressure on the Image
	Rectangle(x, y, 7, 7, 255);
	for (unsigned int i = 0; i < width; i++){
		for (unsigned int j = 0; j < height; j++){
		values(i, j) = (255 - Im(i,j,0,0)) * ( 2 / (1 + ( exp(alpha * dist(x, y, i, j)/sqrt(pow(width,2) + pow(height,2))))));
		Im(i,j,0,0) = 255 - values(i, j);
		}
	}
	Im.save("/home/l/ladharin/MSIAM/PROJECT/Main1/output_images/output_isometric_func1.png");
	Im.display();
}

/**
 * Displays and saves the Image to which the isotropic function exp(-r)*alpha is  applied.
 * @param x the x coordinate of the center of pressure.
 * @param y the y coordinate of the center of pressure.
 * @param alpha the coefficient to tune the function
*/

void Image::Iso_func2(unsigned int x, unsigned int y, float alpha)
{
    Eigen::MatrixXf values(width, height);
    // draw the center of pressure on the Image
    Rectangle(x,y, 7, 7, 255);
    for (unsigned int i = 0; i < width; ++i){
        for (unsigned int j = 0; j < height; j++){
        	values(i, j) = (255 - Im(i,j,0,0))*(pow(exp(- pow((dist(x, y, i, j)/sqrt(pow(width,2) + pow(height,2))),2)),alpha));
        	Im(i,j,0,0) = 255 - values(i, j);
        }
    }
    Im.save("/home/l/ladharin/MSIAM/PROJECT/Main1/output_images/output_isometric_func2.png");
    Im.display();
}

/**
 * Displays and saves the Image to which the isotropic gaussian function is applied
 * @param x the x coordinate of the center of pressure.
 * @param y the y coordinate of the center of pressure.
 * @param alpha the coefficient to tune the function
*/

void Image::Iso_func3(unsigned int x, unsigned int y, float alpha)
{
        Eigen::MatrixXf values(width, height);
	// draw the center of pressure on the Image
        Rectangle(x,y, 7, 7, 255);
        for (unsigned int i = 0; i < width; ++i){
                for (unsigned int j = 0; j < height; j++){
                	values(i, j) =  (255 - Im(i,j,0,0))*(1/(exp(alpha * pow(dist(x, y, i, j),2))));
                	Im(i,j,0,0) = 255 - values(i, j);
                }
        }
        Im.save("/home/l/ladharin/MSIAM/PROJECT/Main1/output_images/output_isometric_func3.png");
        Im.display();
}

/**
  * Displays and saves the Image to which the anisotropic gaussian function is applied
  * @param x the x coordinate of the center of pressure.
  * @param y the y coordinate of the center of pressure.
  * @param alpha the coefficient to tune the function
 */

void Image::Aniso_func(unsigned int x, unsigned int y, float alpha, float a, float b)
{
	Eigen::MatrixXf values(width, height);
	// draw the center of pressure on the Image
        Rectangle(x,y, 5, 5, 255);
	for (unsigned int i = 0; i < width; ++i){
                for (unsigned int j = 0; j < height; j++){
		values(i, j) = (255 - Im(i,j,0,0)) * (1 / (exp(alpha * pow(aniso_dist(x, y, i, j, a, b),2)/(sqrt(pow(width,2) + pow(height,2)))))); 
		Im(i,j,0,0) = 255 - values(i, j);
                }
        }
        Im.save("/output_images/output_anisometric_gaussian.png");
        Im.display();
}





/* 
Function that outputs the symmetrical of the image Im and saves it as "filename"
*/

CImg<unsigned int> Image::ySymmetry(string filename)
{
    CImg<unsigned int> NewImage(width, height, 1, 1);
    for(unsigned int i= 0; i < width; i++){
        for(unsigned int j = 0; j < height; j++){
               NewImage(i, j) = Im(width-i-1, j);
        }
    }
    const char *fileName = filename.c_str();
    return NewImage.save_png(fileName);
}


/*
returns the symmetry of the image along the y-axis without saving it
*/
CImg<unsigned int> Image::ySymmetry()
{
    CImg<unsigned int> NewImage(width, height, 1, 1);
    for(unsigned int i= 0; i < width; i++)
    {
        for(unsigned int j = 0; j < height; j++)
        {
               NewImage(i, j) = Im(width-i-1, j);
        }
    }
    return NewImage;
}



/*
Displays the image Im
*/
void Image::display()
{
    Im.display();
}


/*
Determines the center of the image under the maximum of blackness criteria
*/
pair<int, int> Image::center()
{
    unsigned int ColumnSum = -1;
    unsigned int xOrigin = -1;
    for (int i = 0; i < Im.width(); i++)
    {
        unsigned int Col = -1;
        for (int j = 0; j < Im.height(); j++)
        {
            Col += Im(i, j);
        }
        if (Col < ColumnSum)
        {
            ColumnSum = Col;
            xOrigin = i;
        }
    }
    unsigned int LineSum = -1;
    unsigned int yOrigin = -1;
    for (int i = 0; i < Im.height(); i++)
    {
        unsigned int Line = 0;
        for (int j = 0; j < Im.width(); j++)
        {
            Line += Im(j, i);
        }
        if (Line < LineSum)
        {
            LineSum = Line;
            yOrigin = i;
        }
    }
    return pair<int, int>(xOrigin, yOrigin);
}



/*
Outputs the convenient mask for the image to restore
*/
Masque Image::mask(int PatchSize)
{
    pair<int, int> center = this -> center();
    int x(center.first), y(center.second);
    
    
    
    
    /*
    Determine the radius w.r.t the y-axis
    */
    int yUp;
    int yDown;
    
    int i(0), j(height-1);
    while (Im(x, i) > 0)
    {
        i++;
    }
    yUp = i;
    while (Im(x, j) > 0)
    {
        j--;
    }
    yDown = j;
    
    
    int xLeft;
    int xRight;
    y = (yDown + yUp)/2;
    
    /*
    Determine the radius w.r.t the x-axis
    */
    i = 0;
    j = width-1;
    while (Im(i, y) > 0)
    {
        i++;
    }
    xLeft = i;
    while (Im(j, y) > 0)
    {
        j--;
    }
    xRight = j;
    
    int yRadius = (int)min(y-yUp, yDown-y);
    
    if (yDown > height - 5)
    {
        yRadius = yUp;
    }
    int xRadius = (int)min(x-xLeft, xRight-x);
    
    
    return Masque(PatchSize, width, height, x, y, xRadius, yRadius);
}




/*
Constructs a dictionary with 4 levels of complexity 1 - 2 - 3 - 4
*/
map<int, Patch> Image::dictionary(Masque M, int level)
{
    CImg<unsigned int> SymmIm = this -> ySymmetry();
    
    
    map<int, Patch> dict;
    int indice = 0;
    int PatchSize(M.PatchSize);
    
    Masque SymmM(PatchSize, width, height, width-1-90, 165, 35, 40);
    
    
    std::random_device rd;
    std::mt19937 gen(rd());
    std::normal_distribution<> x(M.xOrigin, width/6);
    std::normal_distribution<> y(M.yOrigin, height/6);
    
    /*
    Cropping from Im
    */
    while (indice < 2000*level)
    {
        int abc(x(gen));
        int ord(y(gen));
        if (pow((abc-M.xOrigin)/55, 2)+pow((ord-M.yOrigin)/90, 2) < 1 and ord < (int)height - (PatchSize-1)/2)
        {
        dict[indice] = Patch(PatchSize, Im, abc, ord);
        indice += 1;
        
        }
    }
    
    
    /*
    Cropping from the symmetrical of Im
    */
    std::normal_distribution<> s(width - 1 - M.xOrigin, width/3);
    while (indice < 3000*level)
    {
        int abc(s(gen));
        int ord(y(gen));
        if (pow((abc-(width-1-M.xOrigin))/55, 2)+pow((ord-M.yOrigin)/90, 2) < 1 and ord < (int)height - (PatchSize-1)/2)
        {
        dict[indice] = Patch(PatchSize, Im, abc, ord);
        indice += 1;
        
        }
    }
   
    
    return dict;
}



/*
Erases all the pixels that must be inpainted again
*/
void Image::clean(Masque &M)
{
    for (float i = 0; i < Im.width(); i++)
    {
        for (float j = 0; j < Im.height(); j++)
        {
            if (pow((i-M.xOrigin)/M.xRadius, 2) + pow((j-M.yOrigin)/M.yRadius, 2) > 1)
            {
                Im(i, j) = 255;
            }
        }
    }
}



/*
Proceeds with the restoration of the image
*/
void Image::restore(map<int, Patch> &dict, Masque &M, int PatchSize)
{   
    int hor = (width/2 - M.xRadius)/10;
    int ver = (height/2 - M.yRadius)/10;
    
    for (int i = 0; i < 10; i++)
    {
        dilate(dict, M, PatchSize, hor, ver);
        Im.display();
    }
}



/*
Principal function of the restoration, a smaller version of the function restore that provides intermediate results
*/
void Image::dilate(map<int, Patch> &dict, Masque &M, int PatchSize, int hor, int ver)
{
    map<unsigned int, vector<pair<unsigned int, unsigned int>>> neighbors;
    
    
    /*
    
    */
    int a(M.xRadius), b(M.yRadius);
    for (float i = 0 ; i < Im.width(); i++)
    {
        for (float j = 0; j < Im.height(); j++)
        {
            if (M.mask(i, j) and pow((i-M.xOrigin)/(a+hor), 2) + pow((j-M.yOrigin)/(b+ver), 2) < 1)
            {
            int dist = pow((i-M.xOrigin)*b/a,2) + pow((j-M.yOrigin), 2);
            neighbors[dist].push_back(pair<unsigned int, unsigned int>(i, j));
            }
        }
    }
    
    cout << dict.size();
    for (int dist = pow(b,2); dist < pow((M.xRadius)*b/a,2) + pow(M.yRadius, 2) + dict.size(); dist++)
    {
        /*
        cout << dist << "  -  " << pow(b+ver, 2) << endl;
        */
        for (unsigned int j = 0; j < neighbors[dist].size(); j++)
        {
            if (M.mask(neighbors[dist][j].first, neighbors[dist][j].second)) 
            {
            Patch P = Patch(PatchSize, Im, neighbors[dist][j].first, neighbors[dist][j].second);
            Patch Ps = P.closestPatch(dict, M.mask);
            Ps.fillWith(Im, M.mask);
            }
        }
        for (unsigned int j = 0; j < neighbors[dist].size(); j++)
        {
            M.mask(neighbors[dist][j].first, neighbors[dist][j].second) = 0;
        }
    }
    M.xRadius += hor;
    M.yRadius += ver;
}






