#include <iostream>
#include <cmath>
#include "Image.h"
#include "Coord.h"
#include "CImg.h"

# define M_PI           3.14159265358979323846  /* pi */
using namespace std;
using namespace cimg_library;

int main(){
	CImg<unsigned int> A("after_rotation1.png");
//	Image img = Image("clean_finger.png");
//	Coord **new_coord = img.Visualize(M_PI / 6, 0.182);
//	Image img1=Image("clean_finger.png");
//	Coord **new_coord1= img1.Visualize(M_PI /6,0.182);
	Inverse_warp(A,M_PI / 6, 0.182);
return 0;
}
